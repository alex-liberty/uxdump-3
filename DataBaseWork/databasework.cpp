#include "databasework.h"

DataBaseWork::DataBaseWork()
{

}

/**
 * @brief DataBaseWork::~DataBaseWork
 * Close database.
 */
DataBaseWork::~DataBaseWork()
{
    dbase.close();
}

/**
 * @brief DataBaseWork::connectBaseMySQL
 * Make connection with database "UXDump" using QMySQL driver.
 * Settings of MySQL Server must be in file "database.ini" in current dirrectory.
 * If database doesn't exist, it will be create.
 */
void DataBaseWork::connectBaseMySQL()
{
    parserIni();
    dbaseName = "UXDump";

    dbase = QSqlDatabase::addDatabase( "QMYSQL" );
    dbase.setHostName( hostName );
    dbase.setPort( port );
    dbase.setUserName( userName );
    dbase.setPassword( password );
    dbase.open();

    if ( dbase.isOpen() ) {
        QSqlQuery create( dbase );
        QString createString = QString( "CREATE DATABASE IF NOT EXISTS %1" ).arg(dbaseName);
        create.prepare( createString );
        if( !create.exec() )
            qDebug() << create.lastError();
    }
    else
        qDebug() << dbase.lastError();

    dbase.close();
    dbase.setDatabaseName( dbaseName );
    dbase.open();
    query = QSqlQuery( dbase );
    createBase();
}

/**
 * @brief DataBaseWork::connectBaseSqlight
 * Make connection with database "UXDump" using QSQLITE driver,
 * file of database will create in current directory of programm, if it doesn't exist.
 */
void DataBaseWork::connectBaseSqlight()
{
    dbase = QSqlDatabase::addDatabase( "QSQLITE" );

    QString pathToDB = QDir::currentPath() + QString( "UXDump.sqlite" );
    dbase.setDatabaseName( pathToDB );
    query = QSqlQuery( dbase );

    dbase.open();
    if ( !dbase.isOpen() ) {
        qDebug() << dbase.lastError();
    }
}

/**
 * @brief DataBaseWork::parserIni
 * Read settings of MySQL server from configuraion file "database.ini", which must be in current directory of aplication,
 * in groupe [Connection] : host name (for local - 127.0.0.1), port (for local - 3306), user name and password.
 */
void DataBaseWork::parserIni()
{
    QString configPath = "./database.ini";
    QFile checkConfig(configPath);

    if( checkConfig.exists() ) {
        QSettings settings(configPath, QSettings::IniFormat);
        settings.beginGroup( "/Connection" );

        hostName    = settings.value( "/Hostname" ).toString();
        port        = settings.value( "/Port" ).toInt();
        userName    = settings.value( "/Username" ).toString();
        password    = settings.value( "/Password" ).toString();

        settings.endGroup();
    }
}

/**
 * @brief DataBaseWork::startTransaction
 * Function start transaction in database for SQL, using before add, update and delete functions.
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::startTransaction()
{
    queryString = "BEGIN";
    return query.exec( queryString );
}

/**
 * @brief DataBaseWork::finishTransaction
 * Function finish trasaction database for SQL, using after add, select, update and delete functions
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::finishTransaction()
{
    queryString = "COMMIT";
    return query.exec( queryString );
}

/**
 * @brief DataBaseWork::lockTables
 * Function lock tables in write database for SQL, using before add, update and delete functions.
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::lockTables()
{
    queryString = "LOCK TABLES a WRITE";
    return query.exec( queryString );
}



bool DataBaseWork::loadFromFile()
{
    queryString =  QString (" LOAD DATA LOCAL INFILE '%1' "
                    "INTO TABLE value "
                    "FIELDS TERMINATED BY ',' "
                    "LINES TERMINATED BY '\n'").arg( QDir::currentPath() + "/forInsert.csv");
    return query.exec( queryString );
}

/**
 * @brief DataBaseWork::unlockTables
 * Function unlock tables in write database for SQL, using after add, select, update and delete functions
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::unlockTables()
{
    queryString = "COMMIT";
    return query.exec( queryString );
}


/**
 * @brief DataBaseWork:: createBase
 * Create all tables of database "UXDump" ( person, test, software, measuring module, log, parameter, value ),
 * if they don't exist, with primary keys, foreing keys, indexes,
 * using InnoDB as engine, charset - utf8.
 */
void DataBaseWork::createBase()
{
    queryString = "CREATE TABLE IF NOT EXISTS person("
                  "id     INTEGER unsigned NOT NULL AUTO_INCREMENT,"
                  "login  VARCHAR ( 150 ) NOT NULL,"
                  "sex    CHAR NOT NULL,"
                  "YOB    YEAR ( 4 ) NOT NULL,"

                  "PRIMARY KEY ( id )"
                  ") ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8;";

    if ( !query.exec( queryString ) )
        qDebug() << "Can not create the table <Person>. " << query.lastError();

    queryString = "CREATE TABLE IF NOT EXISTS test("
                  "id     INTEGER unsigned NOT NULL AUTO_INCREMENT,"
                  "name   VARCHAR ( 150 ) NOT NULL,"

                  "PRIMARY KEY ( id )"
                  ") ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8;";

    if ( !query.exec( queryString ) )
        qDebug() << "Can not create the table <Test>. " << query.lastError();

    queryString = "CREATE TABLE IF NOT EXISTS software("
                  "id         INTEGER unsigned NOT NULL AUTO_INCREMENT,"
                  "name       VARCHAR ( 150 ) NOT NULL, "

                  "PRIMARY KEY ( id )"
                  ") ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8;";

    if ( !query.exec( queryString ) )
        qDebug() << "Can not create the table <Software>. " << query.lastError();

    queryString = "CREATE TABLE IF NOT EXISTS testSoftware("
                  "id               INTEGER unsigned NOT NULL AUTO_INCREMENT,"
                  "idTest           INTEGER unsigned NOT NULL,"
                  "idSoftware       INTEGER unsigned NOT NULL,"

                  "PRIMARY KEY ( id ),"

                  "FOREIGN KEY ( idTest ) REFERENCES test ( id )"
                  "ON UPDATE CASCADE "
                  "ON DELETE CASCADE, "

                  "FOREIGN KEY ( idSoftware ) REFERENCES software ( id )"
                  "ON UPDATE CASCADE "
                  "ON DELETE CASCADE"

                  ") ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8;";

    if ( !query.exec( queryString ) )
        qDebug() << "Can not create the table <testSoftware>. " << query.lastError();


    queryString = "CREATE TABLE IF NOT EXISTS measuringModule("
                  "id     INTEGER unsigned NOT NULL AUTO_INCREMENT,"
                  "name   VARCHAR ( 150 ) NOT NULL,"
                  "PRIMARY KEY ( id )"
                  ") ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8;";

    if ( !query.exec( queryString ) )
        qDebug() << "Can not create the table <Measuring modules>. " << query.lastError();

    queryString = "CREATE TABLE IF NOT EXISTS log("
                  "id                     INTEGER unsigned NOT NULL AUTO_INCREMENT,"
                  "idPerson               INTEGER unsigned NOT NULL,"
                  "idTestSoftware         INTEGER unsigned NOT NULL,"
                  "idMeasuringModule      INTEGER unsigned NOT NULL,"
                  "dateTime               VARCHAR ( 23 ) NOT NULL,"
                  "duration               INTEGER NOT NULL,"
                  "note                   VARCHAR ( 128 ) NOT NULL,"

                  "PRIMARY KEY ( id ),"

                  "FOREIGN KEY ( idPerson ) REFERENCES person ( id )"
                  "ON UPDATE CASCADE "
                  "ON DELETE CASCADE ,"

                  "FOREIGN KEY ( idTestSoftware ) REFERENCES testSoftware ( id )"
                  "ON UPDATE CASCADE "
                  "ON DELETE CASCADE ,"

                  "FOREIGN KEY ( idMeasuringModule ) REFERENCES measuringModule( id )"
                  "ON UPDATE CASCADE "
                  "ON DELETE CASCADE "
                  ") ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8;";

    if ( !query.exec( queryString ) )
        qDebug() << "Can not create the table <Log>. " << query.lastError();

    queryString = "CREATE TABLE IF NOT EXISTS parameter("
                  "id            INTEGER unsigned NOT NULL AUTO_INCREMENT,"
                  "name          VARCHAR(150) NOT NULL,"

                  "PRIMARY KEY ( id )"
                  ") ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8;";

    if ( !query.exec( queryString ) )
        qDebug() << "Can not create the table <Parameter>. " << query.lastError();

    queryString = "CREATE TABLE IF NOT EXISTS value("
                  "id             INTEGER unsigned NOT NULL AUTO_INCREMENT,"
                  "idLog          INTEGER unsigned NOT NULL,"
                  "idParameter    INTEGER unsigned NOT NULL,"
                  "data           VARCHAR ( 150 ) NOT NULL,"

                  "PRIMARY KEY ( id ),"

                  "FOREIGN KEY ( idLog ) REFERENCES log ( id )"
                  "ON UPDATE CASCADE "
                  "ON DELETE CASCADE ,"

                  "FOREIGN KEY ( idParameter ) REFERENCES parameter ( id )"
                  "ON UPDATE CASCADE "
                  "ON DELETE CASCADE "

                  ") ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8;";

    if ( !query.exec( queryString ) )
        qDebug() << "Can not create the table <Value>. " << query.lastError();
}

/**
 * @brief DataBaseWork::selectPersonId
 * Select note's id from table "Person" using login.
 * @param inputLogin Value, whith what make compare.
 * @return id of note.
 */
int DataBaseWork::selectPersonId( QString inputLogin )
{
    queryString = QString ( "SELECT id FROM person WHERE login = '%1'" ).arg( inputLogin );
    query = QSqlQuery ( queryString );
    query.next();

    return query.value( 0 ).toInt();
}

/**
 * @brief DataBaseWork::selectTestId
 * Select note's id from table "Test" using name.
 * @param inputName Value, whith what make compare.
 * @return id of note.
 */
int DataBaseWork::selectTestId( QString inputName )
{
    queryString = QString ( "SELECT id FROM test WHERE name = '%1'" ).arg( inputName );
    query = QSqlQuery ( queryString );
    query.next();

    return query.value( 0 ).toInt();
}

/**
 * @brief DataBaseWork::selectSoftwareId
 * Select note's id from table "Software" using name.
 * @param inputName Value, whith what make compare.
 * @return id of note.
 */
int DataBaseWork::selectSoftwareId( QString inputName )
{
    queryString = QString ( "SELECT id FROM software WHERE name = '%1'" ).arg( inputName );
    query = QSqlQuery ( queryString );
    query.next();

    return query.value( 0 ).toInt();
}

/**
 * @brief DataBaseWork::selectSoftwareId
 * Select note's id from table "Software" using name and id of test.
 * @param inputName Value, whith what make compare.
 * @return id of note.
 */
int DataBaseWork::selectTestSoftwareId()
{
    queryString = QString ( "SELECT id FROM testSoftware WHERE idTest = %1 AND idSoftware = %2" ).
            arg( idTest ).arg( idSoftware );
    query = QSqlQuery ( queryString );
    query.next();

    return query.value( 0 ).toInt();
}

/**
 * @brief DataBaseWork::selectMeasuringModuleId
 * Select note's id from table "Measuring module" using name and id of test
 * @param inputName Value, whith what make compare.
 * @return id of note.
 */
int DataBaseWork::selectMeasuringModuleId( QString inputName )
{
    queryString = QString ( "SELECT id FROM measuringModule WHERE name = '%1'" ).arg( inputName );
    query = QSqlQuery ( queryString );
    query.next();

    return query.value( 0 ).toInt();
}

int DataBaseWork::selectParameter( QString inputName )
{
    queryString = QString ( "SELECT id FROM parameter WHERE name = '%1'" ).arg( inputName );
    query = QSqlQuery ( queryString );
    query.next();

    return query.value( 0 ).toInt();
}

/**
 * @brief DataBaseWork::addPerson
 * Add new note in table "Person" using name, sex and year of bith
 * @param login Value of new note in column "Login",
 * @param sex Value of new note in column "Sex",
 * @param YOB Value of new note in column "YOB".
 * @return id of new note
 */
int DataBaseWork::addPerson( QString login, QString sex, int YOB )
{
    queryString = QString( "INSERT INTO person ( login, sex, YOB )"
                           " VALUES ( '%1', '%2', %3 )" ).arg( login ).arg( sex ).arg( YOB );

    bool result = query.exec( queryString );

    if ( !result ) {
        qDebug() << "Can not insert item in <<Person>> now. " << query.lastError();
        return 0;
    }

    else
        return maxPersonId();
}

/**
 * @brief DataBaseWork::addTest
 * Add new note in table "Test" using name.
 * @param name Value of new note in column "Name".
 * @return id of new note.
 */
int DataBaseWork::addTest( QString name )
{
    queryString = QString( "INSERT INTO test( name )"
                           " VALUES ('%1')" ).arg( name );

    bool result = query.exec( queryString );

    if ( !result ) {
        qDebug() << "Can not insert item in <<Test>> now. " << query.lastError();
        return 0;
    }

    else
        return maxTestId();
}

/**
 * @brief DataBaseWork::addSoftware
 * Add new note in table "Software" using name and test.
 * @param name Value of new note in column "Name".
 * @param test Value of new note in column "idTest", with what associated current software
 * @return id of new note.
*/
int DataBaseWork::addSoftware( QString name )
{
    queryString = QString( "INSERT INTO software( name )"
                           " VALUES ('%1')" ).arg( name );

    bool result = query.exec( queryString );

    if ( !result ) {
        qDebug() << "Can not insert item in <<Software>> now. " << query.lastError();
        return 0;
    }

    else
        return maxSoftwareID();
}

/**
 * @brief DataBaseWork::addTestSoftware
 * Add new note in table "Test Software" using name of test and software.
 * @param test Value of new note in column "idTest", with what associated current note
 * @param software Value of new note in column "idSoftware", with what associated current note
 * @return id of new note.
 */
int DataBaseWork::addTestSoftware( )
{
    queryString = QString( "INSERT INTO testSoftware( idTest, idSoftware )"
                           " VALUES (%1, %2)" ).arg( idTest ).arg( idSoftware );


    bool result = query.exec( queryString );

    if ( !result ) {
        qDebug() << "Can not insert item in <<Test Software>> now. " << query.lastError();
        return 0;
    }

    else
        return maxTestSoftwareID();
}

/**
 * @brief DataBaseWork::addMeasuringModule
 * Add new note in table "Measuring module" using name.
 * @param name Value of new note in column "Name".
 * @return id of new note.
 */
int DataBaseWork::addMeasuringModule( QString name )
{
    queryString = QString( "INSERT INTO measuringModule ( name ) "
                           "VALUES ( '%1')" ).arg( name );

    bool result = query.exec( queryString );

    if ( !result ) {
        qDebug() << "Can not insert item in <<Measuring Module>> now. " << query.lastError();
        return 0;
    }

    else
        return maxMeasuringModuleId();
}

/**
 * @brief DataBaseWork::addLog
 * Add new note in table "Log" using id of notes: person, test, software, measuring module - and values of dateTime, duration of expirement and note
 * @param person Value, what associated current note with note in table "Person", using field "id",
 * @param test Value, what associated current note with note in table "Test", using field "id",
 * @param software Value, what associated current note with note in table "Software", using field "id",
 * @param measuringModule Value, what associated current note with note in table "Measuring module", using field "id".
 * @return id of new note.
 */
int DataBaseWork::addLog( int person, int testSoftware, int measuringModule,
                          QString dateTime, int duration, QString note )
{
    queryString = QString( "INSERT INTO log( idPerson, idTestSoftware, idMeasuringModule, "
                           "dateTime, duration, note ) "
                           "VALUES ( %1, %2, %3, '%4', %5, '%6')" ).
            arg( person ).arg( testSoftware ).arg( measuringModule ).
            arg( dateTime ).arg( duration ).arg( note );

    bool result = query.exec( queryString );

    if ( !result ) {
        qDebug() << "Can not insert item in <<Log>> now. " << query.lastError();
        return 0;
    }

    else
        return maxLogId();
}

/**
 * @brief DataBaseWork::addLog
 * Add new note in table "Log" using fields of notes: person, test, software, measuring module - and values of dateTime, duration of expirement and note
 * @param person Value, what associated current note with note in table "Person", using field "login",
 * @param sex Value, what associated current note with note in table "Person", using field "sex",
 * @param YOB Value, what associated current note with note in table "Person", using field "YOB",
 * @param test Value, what associated current note with note in table "Test", using field "name",
 * @param software Value, what associated current note with note in table "Software", using field "name",
 * @param measuringModule Value, what associated current note with note in table "Measuring module", using field "name".
 * @return - id of new note.
 */
int DataBaseWork::addLog( QString person, QString sex, int YOB, QString test, QString software,
                          QString measuringModule, QString dateTime, int duration,  QString note )
{
    idPerson = selectPersonId( person );
    if ( !idPerson )
        idPerson = addPerson( person, sex, YOB );
    idTest = selectTestId( test );
    if ( !idTest )
        idTest = addTest( test );

    idSoftware = selectSoftwareId( software );
    if ( !idSoftware )
        idSoftware = addSoftware( software );
    
    idTestSoftware = selectTestSoftwareId();
    if ( !idTestSoftware )
        idTestSoftware = addTestSoftware();


    idMeasuringModule = selectMeasuringModuleId( measuringModule );

    if ( !idMeasuringModule )
        idMeasuringModule = addMeasuringModule( measuringModule );

    return addLog( idPerson, idTestSoftware, idMeasuringModule, dateTime, duration, note );
}

/**
 * @brief DataBaseWork::addParameter
 * Add new note in table "Parameter" using name, log and number of column in log-file.
 * @param name Value of new note in column "Name",
 * @return id of new note.
 */
int DataBaseWork::addParameter(QString name )
{
    idParameter = selectParameter( name );

    if ( idParameter != 0 )
        return idParameter;

    queryString = QString( "INSERT INTO parameter ( name ) "
                           "VALUES ( '%1')" ).arg( name );

    bool result = query.exec( queryString );

    if ( !result ) {
        qDebug() << "Can not insert item in <<Parameter>> item now. " << query.lastError();
        return 0;
    }

    else
        return maxParameterId();
}

/**
 * @brief DataBaseWork::addValue
 * Add new note in table "Value" using data, log and number of column in log-file.
 * @param data Value of new note in column "Data",
 * @param log  Value of new note in column "idLog", what associated current note with note in table "Log", using field "id",
 * @param numberColumn Value, which shows number of column with this parameter in log - file
 * @return id of new note
 */
int DataBaseWork::addValue( int log, QString parameter, QString data )
{
    idParameter = selectParameter( parameter );

    if ( !idParameter )
        idParameter = addParameter( parameter );

    queryString = QString( "INSERT INTO value ( idLog, idParameter, data ) "
                           "VALUES ( %1, %2, '%3')" ).
            arg( log ).arg( idParameter ).arg( data );

    bool result = query.exec( queryString );

    if ( !result ) {
        qDebug() << "Can not insert in <<Value>> item now. " << query.lastError();
        return 0;
    }

    else
        return maxValueId();
}

/**
 * @brief DataBaseWork::selectPersonLogin
 * Select all user logins in table "Person".
 * @return list of logins.
 */
QStringList DataBaseWork::selectPersonLogin()
{
    QStringList loginList;
    query = QSqlQuery ( "SELECT login FROM person" );

    while ( query.next() )
        loginList << query.value( 0 ).toString();

    return loginList;
}

/**
 * @brief DataBaseWork::selectTestName
 * Select all test names in table "Test".
 * @return list of test's names.
 */
QStringList DataBaseWork::selectTestName()
{
    QStringList nameList;
    query = QSqlQuery ( "SELECT name FROM test" );

    while ( query.next() )
        nameList << query.value( 0 ).toString();

    return nameList;
}

/**
 * @brief DataBaseWork::selectSoftwareName
 * Select all software names in table "Software".
 * @return list of software names.
 */
QStringList DataBaseWork::selectSoftwareName()
{
    QStringList nameList;
    query = QSqlQuery ( "SELECT name FROM software" );

    while ( query.next() )
        nameList << query.value( 0 ).toString();

    return nameList;
}

/**
 * @brief DataBaseWork::selectDistinctSoftwareName
 * Select distinct software names in table "Software".
 * @return list of distinct software names.
 */
QStringList DataBaseWork::selectDistinctSoftwareName()
{
    QStringList nameList;
    query = QSqlQuery ( "SELECT DISTINCT name FROM software" );

    while ( query.next() )
        nameList << query.value( 0 ).toString();

    return nameList;
}

/**
 * @brief DataBaseWork::selectSoftwareName
 * Select all software names, which associated with test name.
 * @param inputTestName Value, with associated whith test's name in table "Test".
 * @return list of all software names, witch answer conditions of query.
 */
QStringList DataBaseWork::selectSoftwareName( QString inputTestName )
{
    QStringList softwareList;
    idTest = selectTestId( inputTestName );
    queryString = QString ( "SELECT name FROM software WHERE idTest = %1" ).arg( idTest );
    query = QSqlQuery ( queryString );

    while( query.next() )
        softwareList.append( query.value( 0 ).toString() );

    return softwareList;
}

/**
 * @brief DataBaseWork::selectMeasuringModuleName
 * Select all measuring module names in table "Measuring Module".
 * @return list of module names.
 */
QStringList DataBaseWork::selectMeasuringModuleName()
{
    QStringList nameList;
    query = QSqlQuery ( "SELECT name FROM measuringModule" );

    while ( query.next() )
        nameList << query.value( 0 ).toString();

    return nameList;
}

/**
 * @brief DataBaseWork::selectDistinctDateTime
 * Select distinct values of DateTime in table "Log".
 * @return list of string values.
 */
QStringList DataBaseWork::selectDistinctDateTime()
{
    QStringList dateTimeList;
    query = QSqlQuery ( "SELECT DISTINCT dateTime FROM log" );

    while ( query.next() )
        dateTimeList << query.value( 0 ).toString();

    return dateTimeList;
}

/**
 * @brief DataBaseWork::selectDateTime
 * Select all values of DateTime in table "Log".
 * @return list of string values DateTime.
 */
QStringList DataBaseWork::selectDateTime( int inputLog )
{
    QStringList dateTimeList;
    queryString = QString ( "SELECT dateTime FROM log WHERE id = %1" ).arg( inputLog );
    query = QSqlQuery ( queryString );

    while ( query.next() )
        dateTimeList.append( query.value( 0 ).toString() );

    return dateTimeList;
}

/**
 * @brief DataBaseWork::selectDistinctParameterName
 * Select distinct parameter names in table "Parameter".
 * @return list of parameter names.
 */
QStringList DataBaseWork::selectDistinctParameterName()
{
    QStringList nameList;
    query = QSqlQuery ( "SELECT DISTINCT name FROM parameter" );

    while ( query.next() )
        nameList << query.value( 0 ).toString();

    return nameList;
}

/**
 * @brief DataBaseWork::selectValue
 * Select all notes from tables, witch answer conditions of query.
 * If one of these input values is empty, it means that parameter doesn't ..........
 * @param measuringModuleInput Value measuring module's name, which influence to selection notes from table "Measuring Module",
 * @param testInput Value test's name, which influence to selection notes from table "Test",
 * @param softwareInput Value software's name, which influence to selection notes from table "Software",
 * @param personInput Value person's login, which influence to selection notes from table "Person",
 * @param parameterInput Value parameter's name, which influence to selection notes from table "Parameter",
 * @param dateTimeInput Value dateTime of log, which influence to selection notes from table "Log".
 * @return all values in string format, except id of notes.
 */
QList <QStringList> DataBaseWork::selectValue( QString measuringModuleInput, QString testInput, QString softwareInput,
                                               QString personInput, QString parameterInput, QString dateTimeInput )
{
    QList<QStringList> output;
    QStringList dataOutput;
    QStringList idParameterOutput;
    QStringList idLogOutput;

    QStringList personLoginOutput;
    QStringList personSexOutput;
    QStringList personYOBOutput;

    QStringList testNameOutput;
    QStringList softwareNameOutput;
    QStringList measuringModuleOutput;
    QStringList parameterNameOutput;

    QStringList logPersonOutput;
    QStringList logTestOutput;
    QStringList logSoftwareOutput;
    QStringList logModuleOutput;
    QStringList logDurationOutput;
    QStringList logDateTimeOutput;

    QString logFilter;
    QString parameterFilter;
    QString personFilter;
    QString testFilter;
    QString softwareFilter;
    QString measuringModuleFilter;
    QString testSoftwareFilter;

    QSqlQuery functionQuery = QSqlQuery( dbase );

    //Строки запроса к каждой простой сущности
    personFilter = QString( "SELECT person.id FROM person" );

    if ( !personInput.isEmpty() )
        personFilter += QString( " WHERE person.login = '%1'").arg( personInput );

    testFilter = QString( "SELECT test.id FROM test" );

    if ( !testInput.isEmpty() )
        testFilter += QString( " WHERE test.name = '%1'").arg( testInput );

    softwareFilter = QString( "SELECT software.id FROM software" );

    if (  !softwareInput.isEmpty() )
        softwareFilter += QString( " WHERE software.name = '%1'").arg( softwareInput );

    measuringModuleFilter = QString( "SELECT measuringModule.id FROM measuringModule" );

    if ( !measuringModuleInput.isEmpty() )
        measuringModuleFilter += QString( " WHERE measuringModule.name = '%1'").arg( measuringModuleInput );

    parameterFilter = QString( "SELECT parameter.id FROM parameter");

    if ( !parameterInput.isEmpty() )
        parameterFilter += QString( " WHERE parameter.name = '%1'" ).arg( parameterInput );

    //Строки запроса к промежуточным сущностям - testSoftware
    testSoftwareFilter = QString( "SELECT testSoftware.id FROM testSoftware "
                                  "WHERE testSoftware.idTest IN (");
    testSoftwareFilter += testFilter;
    testSoftwareFilter += QString( ") " );
    testSoftwareFilter += QString( "AND testSoftware.idSoftware IN (");
    testSoftwareFilter += softwareFilter;
    testSoftwareFilter += QString( ")" );

    //Строка запроса к таблице логов
    logFilter = QString( "SELECT log.id FROM log  WHERE" );

    if ( !dateTimeInput.isEmpty() )
        logFilter += QString (" log.dateTime = '%1' AND").arg( dateTimeInput );

    logFilter += QString( " log.idPerson IN (");
    logFilter += personFilter;
    logFilter += QString( ")" );
    logFilter += QString( " AND" );

    logFilter += QString( " log.idTestSoftware IN (" );
    logFilter += testSoftwareFilter;
    logFilter += QString( ")" );
    logFilter += QString( " AND" );

    logFilter += QString( " log.idMeasuringModule IN (" );
    logFilter += measuringModuleFilter;
    logFilter += QString( ")" );

    //Строка запроса к VALUE
    queryString = QString( "SELECT value.data, value.idParameter, value.idLog FROM value "
                           "WHERE value.idLog IN ( " );
    queryString += logFilter;
    queryString += QString( " )" );

    queryString += QString( " AND value.idParameter IN ( ");
    queryString += parameterFilter;
    queryString += QString( " )" );

    //qDebug() << queryString;
    query = QSqlQuery ( queryString );
    while ( query.next() ) {
        dataOutput << query.value( 0 ).toString();
        idParameterOutput << query.value( 1 ).toString();
        idLogOutput << query.value( 2 ).toString();
    }

    query = QSqlQuery ( personFilter );

    while ( query.next() ) {
        idPerson = query.value( 0 ).toInt();
        queryString = QString( "SELECT person.login, person.sex, person.YOB FROM person "
                               "WHERE person.id = %1").arg( query.value( 0 ).toInt() );
        functionQuery = QSqlQuery( queryString );

        while( functionQuery.next() ) {
            personLoginOutput << functionQuery.value( 0 ).toString();
            personSexOutput << functionQuery.value( 1 ).toString();
            personYOBOutput << functionQuery.value( 2 ).toString();
        }
    }

    functionQuery = QSqlQuery ( testFilter );

    while ( functionQuery.next() )
        testNameOutput << selectTestName( functionQuery.value( 0 ).toInt() );

    query = QSqlQuery ( softwareFilter );

    while ( query.next() ) {
        softwareNameOutput << selectSoftwareName( query.value( 0 ).toInt() );
    }

    functionQuery = QSqlQuery ( measuringModuleFilter );

    while ( functionQuery.next() )
        measuringModuleOutput << selectMeasuringModuleName( functionQuery.value( 0 ).toInt() );

    functionQuery = QSqlQuery ( parameterFilter );

    while ( functionQuery.next() )
        parameterNameOutput << selectParameterName( functionQuery.value( 0 ).toInt() );

    //Итак, мой милый друг, тут завелся монстр, прыгающий на костыле...
    //Надо все ж разобраться на кой черт на нужен этот код
    //и че он возвращает-то и зачем... главное зачем он это возвращает...
    // А за окном шумит дождь... романтика... можно лежать в пледике и пить какаушко... но я на работе
    //А не... под хор кубанского казачества - все норм, нужен нам этот код, он заполняет таблицу с логами не ерундой
    query = QSqlQuery( logFilter );

    while ( query.next() ){
        queryString = QString( "SELECT person.login, test.name, software.name, measuringModule.name, log.duration, log.dateTime "
                               "FROM person, test, software, measuringModule, log "
                               "WHERE "
                               "person.id IN(SELECT log.idPerson FROM log WHERE log.id = %1) "
                               "AND test.id IN( SELECT testSoftware.idTest FROM testSoftware WHERE testSoftware.id IN ("
                               "SELECT log.idTestSoftware FROM log WHERE log.id = %1)) "
                               "AND software.id IN( SELECT testSoftware.idSoftware FROM testSoftware WHERE testSoftware.id IN ("
                               "SELECT log.idTestSoftware FROM log WHERE log.id = %1)) "
                               "AND measuringModule.id IN(SELECT log.idMeasuringModule FROM log WHERE log.id = %1) "
                               "AND log.id = %1"
                               ).arg( query.value( 0 ).toInt() );
        functionQuery = QSqlQuery( queryString );

        while( functionQuery.next() ) {
            logPersonOutput << functionQuery.value( 0 ).toString();
            logTestOutput << functionQuery.value( 1 ).toString();
            logSoftwareOutput << functionQuery.value( 2 ).toString();
            logModuleOutput << functionQuery.value( 3 ).toString();
            logDurationOutput << functionQuery.value( 4 ).toString();
            logDateTimeOutput << functionQuery.value( 5 ).toString();
        }
    }

    output << dataOutput << idParameterOutput << idLogOutput <<
              personLoginOutput << personSexOutput << personYOBOutput <<
              testNameOutput <<
              measuringModuleOutput <<
              softwareNameOutput <<
              logPersonOutput << logTestOutput << logSoftwareOutput << logModuleOutput << logDurationOutput << logDateTimeOutput <<
              parameterNameOutput;

    return output;
}

/**
 * @brief DataBaseWork::deleteLog
 * Delete notes from tables "Log", "Parameter", "Value" using id of log, and update values in them.
 * @param inputId Value id, which influence to selection notes from tables.
 * @return true if query was made correct, else - false.
 */
bool DataBaseWork::deleteLog( int inputId )
{
    queryString = QString( "DELETE FROM log WHERE id = %1" ).arg( inputId );
    result = query.exec( queryString );

    if ( result ) {
        updateLogId();
        updateParameterId();
        updateValueId();
    }

    return result;
}

/**
 * @brief DataBaseWork::selectPersonLogin
 * Select one item of person's login in table "Person", using id.
 * @param idInput Value person's id, which influence to selection note.
 * @return one string value of note from column "Login".
 */
QString DataBaseWork::selectPersonLogin( int idInput )
{
    queryString = QString( "SELECT login FROM person WHERE id = %1" ).arg( idInput );
    query = QSqlQuery ( queryString );
    query.next();

    return query.value( 0 ).toString();
}

/**
 * @brief DataBaseWork::selectPersonSex
 * Select one item of person's sex in table "Person", using login.
 * @param personLogin Value person's login, which influence to selection note.
 * @return one string value of note from column "Sex".
 */
QString DataBaseWork::selectPersonSex( QString personLogin )
{
    resultSelect = selectPersonId( personLogin );

    if ( resultSelect == 0 )
        return "";

    queryString = QString ( "SELECT sex FROM person WHERE id = %1" ).arg( resultSelect );
    query = QSqlQuery ( queryString );
    query.next();

    return query.value( 0 ).toString();
}

/**
 * @brief DataBaseWork::selectPersonYOB
 * Select one item of person's year of birth in table "Person", using login.
 * @param personLogin Value person's login, which influence to selection note.
 * @return one string value of note from column "YOB".
 */
QString DataBaseWork::selectPersonYOB( QString personLogin )
{
    resultSelect = selectPersonId( personLogin );

    if ( resultSelect == 0 )
        return "";

    queryString = QString ( "SELECT YOB FROM person WHERE id = %1" ).arg( resultSelect );
    query = QSqlQuery ( queryString );
    query.next();

    return query.value( 0 ).toString();
}

/**
 * @brief DataBaseWork::selectTestName
 * Select one item of test's name in table "Test", using id.
 * @param idInput Value test's id, which influence to selection note.
 * @return one string value of note from column "Name".
 */
QString DataBaseWork::selectTestName( int idInput )
{
    queryString = QString( "SELECT name FROM test WHERE id = %1" ).arg( idInput );
    query = QSqlQuery ( queryString );

    query.next();

    return query.value( 0 ).toString();
}

/**
 * @brief DataBaseWork::selectSoftwareName
 * Select one item of software's name in table "Software", using id.
 * @param idInput Value software's id, which influence to selection note.
 * @return one string value of note from column "Name".
 */
QString DataBaseWork::selectSoftwareName( int idInput )
{
    queryString = QString( "SELECT name FROM software WHERE id = %1" ).arg( idInput );
    query = QSqlQuery( queryString );
    query.next();

    return query.value( 0 ).toString();
}

/**
 * @brief DataBaseWork::selectTestUsingSoftware
 * Select one item of software's name in table "Test", using software's id.
 * @param idInput Value software's id, which influence to selection note.
 * @return one string value of note from column "Name".
 */
QString DataBaseWork::selectTestUsingSoftware( int idInput )
{
    queryString = QString( "SELECT name FROM test WHERE id IN (SELECT id FROM software WHERE id = %1)" ).arg( idInput );
    query = QSqlQuery( queryString );
    query.next();

    return query.value( 0 ).toString();
}

/**
 * @brief DataBaseWork::selectMeasuringModuleName
 * Select one item of measuring module's name in table "Measuring Module", using id.
 * @param idInput Value measuring module's id, which influence to selection note.
 * @return one string value of note from column "Name".
 */
QString DataBaseWork::selectMeasuringModuleName( int idInput )
{
    queryString = QString( "SELECT name FROM measuringModule WHERE id = %1" ).arg( idInput );
    query = QSqlQuery ( queryString );
    query.next();

    return query.value( 0 ).toString();;
}

/**
 * @brief DataBaseWork::selectParameterName
 * Select one item of parameter's name in table "Parameter", using id.
 * @param idInput Value measuring module's id, which influence to selection note.
 * @return one string value of note from column "Name".
 */
QString DataBaseWork::selectParameterName( int idInput )
{
    queryString = QString( "SELECT name FROM parameter WHERE id = %1" ).arg( idInput );
    query = QSqlQuery ( queryString );
    query.next();

    return query.value( 0 ).toString();;
}


/**
 * @brief DataBaseWork::selectLog
 * Select one item of log's id in table "Log", using foreign keys.
 * @param inputModule String value measuring module's name, which influence to selection id of note in table "Measuring module",
 * @param inputTest String value test's name, which influence to selection id of note in table "Test",
 * @param inputSoftware String value software's name, which influence to selection id of note in table "Software",
 * @param inputLogin String value measuring module's name, which influence to selection id of note in table "Person",
 * @param inputDateTime String value dateTime of log, which influence to selection id of note in table "Log"
 * @return one int value of note from column "Id".
 */
int DataBaseWork::selectLog( QString inputModule, QString inputTest, QString inputSoftware,
                             QString inputLogin, QString inputDateTime )
{
    idMeasuringModule = selectMeasuringModuleId( inputModule );
    idTest = selectTestId( inputTest );
    idSoftware = selectSoftwareId( inputSoftware );
    idPerson = selectPersonId( inputLogin );

    queryString = QString ( "SELECT id FROM log "
                            "WHERE idMeasuringModule = %1 AND idTest = %2 AND idSoftware = %3 AND idPerson = %4 AND dateTime = '%5'" ).
            arg( idMeasuringModule ).arg( idTest ).arg( idSoftware ).arg( idPerson ).arg( inputDateTime );
    query = QSqlQuery ( queryString );
    query.next();

    return query.value( 0 ).toInt();
}

/**
 * @brief DataBaseWork::selectLog
 * Select all items of log's id in table "Log", using foreign keys.
 * @param inputModule String value measuring module's name, which influence to selection id of note in table "Measuring module",
 * @param inputTest String value test's name, which influence to selection id of note in table "Test",
 * @param inputSoftware String value software's name, which influence to selection id of note in table "Software",
 * @param inputLogin String value measuring module's name, which influence to selection id of note in table "Person",
 * @return all int value of notes from column "Id".
 */
QList <int> DataBaseWork::selectLog( QString inputModule, QString inputTest, QString inputSoftware, QString inputLogin )
{
    QList <int> logList;
    idMeasuringModule = selectMeasuringModuleId( inputModule );
    idTest = selectTestId( inputTest );
    idSoftware = selectSoftwareId( inputSoftware );
    idPerson = selectPersonId( inputLogin );
    queryString = QString ( "SELECT id FROM log "
                            "WHERE idMeasuringModule = %1 AND idTest = %2 AND idSoftware = %3 AND idPerson = %4" ).
            arg( idMeasuringModule ).arg( idTest ).arg( idSoftware ).arg( idPerson );
    query = QSqlQuery ( queryString );

    while ( query.next() )
        logList.append( query.value( 0 ).toInt() );

    return logList;
}

/**
 * @brief DataBaseWork::maxPersonId.
 * Select max value of id in table "Person".
 * @return result of select as integer value.
 */
int DataBaseWork::maxPersonId()
{
    query = QSqlQuery ( "SELECT MAX( id ) FROM person" );
    query.next();

    return query.value( 0 ).toInt();
}

/**
 * @brief DataBaseWork::maxTestId
 * Select max value of id in table "Test".
 * @return result of select as integer value.
 */
int DataBaseWork::maxTestId()
{
    query = QSqlQuery ( "SELECT MAX( id ) FROM test" );
    query.next();

    return query.value( 0 ).toInt();
}

/**
 * @brief DataBaseWork::maxSoftwareID
 * Select max value of id in table "Software".
 * @return result of select as integer value.
 */
int DataBaseWork::maxSoftwareID()
{
    query = QSqlQuery ( "SELECT MAX( id ) FROM software" );
    query.next();

    return query.value( 0 ).toInt();
}

/**
 * @brief DataBaseWork::maxTestSoftwareID
 * Select max value of id in table "Software".
 * @return result of select as integer value.
 */
int DataBaseWork::maxTestSoftwareID()
{
    query = QSqlQuery ( "SELECT MAX( id ) FROM testSoftware" );
    query.next();

    return query.value( 0 ).toInt();
}

/**
 * @brief DataBaseWork::maxMeasuringModuleId
 * Select max value of id in table "Measuring module".
 * @return result of select as integer value.
 */
int DataBaseWork::maxMeasuringModuleId()
{
    query = QSqlQuery ( "SELECT MAX( id ) FROM measuringModule" );
    query.next();

    return query.value( 0 ).toInt();
}

/**
 * @brief DataBaseWork::maxLogId
 * Select max value of id in table "Log".
 * @return result of select as integer value.
 */
int DataBaseWork::maxLogId()
{
    query = QSqlQuery ( "SELECT MAX( id ) FROM log" );
    query.next();

    return query.value( 0 ).toInt();
}

/**
 * @brief DataBaseWork::maxParameterId
 * Select max value of id in table "Parameter".
 * @return result of select as integer value.
 */
int DataBaseWork::maxParameterId()
{
    query = QSqlQuery ( "SELECT MAX( id ) FROM parameter" );
    query.next();

    return query.value( 0 ).toInt();
}

/**
 * @brief DataBaseWork::maxValueId
 * Select max value of id in table "Value".
 * @return result of select as integer value.
 */
int DataBaseWork::maxValueId()
{
    query = QSqlQuery ( "SELECT MAX( id ) FROM value" );
    query.next();

    return query.value( 0 ).toInt();
}

/**
 * @brief DataBaseWork::selectSoftwareUsingTest
 * Select all item of softwares' id in table "Software", using test's id.
 * @param inputId Integer value test's id, which influence to selection id of note in table "Software",
 * @return all integer value of  selected note from column "Id".
 */
QList <int> DataBaseWork::selectSoftwareUsingTest( int inputId )
{
    QList <int> softwareList;
    queryString = QString ( "SELECT id FROM software WHERE idTest = %1" ).arg( inputId );
    query = QSqlQuery ( queryString );

    while ( query.next() )
        softwareList.append( query.value( 0 ).toInt() );

    return softwareList;
}

/**
 * @brief DataBaseWork::selectLogUsingPerson
 * Select all item of logs' id in table "Log", using person's id.
 * @param inputId Integer value person's id, which influence to selection id of note in table "Log",
 * @return all integer value of  selected note from column "Id".
 */
QList <int> DataBaseWork::selectLogUsingPerson( int inputId )
{
    QList <int> logList;
    queryString = QString ( "SELECT id FROM log WHERE idPerson = %1" ).arg( inputId );
    query = QSqlQuery ( queryString );

    while ( query.next() )
        logList.append( query.value( 0 ).toInt() );

    return logList;
}

/**
 * @brief DataBaseWork::selectLogUsingTest
 * Select all item of logs' id in table "Log", using person's id.
 * @param inputId Integer value test's id, which influence to selection id of note in table "Log",
 * @return all integer value of  selected note from column "Id".
 */
QList <int> DataBaseWork::selectLogUsingTest ( int inputId )
{
    QList <int> logList;
    queryString = QString ( "SELECT id FROM log WHERE idTest = %1" ).arg( inputId );
    query = QSqlQuery ( queryString );

    while ( query.next() )
        logList.append( query.value( 0 ).toInt() );

    return logList;
}

/**
 * @brief DataBaseWork::selectLogUsingSoftware
 * Select all item of logs' id in table "Log", using test's id.
 * @param inputId Integer value software's id, which influence to selection id of note in table "Log",
 * @return all integer value of  selected note from column "Id".
 */
QList <int> DataBaseWork::selectLogUsingSoftware ( int inputId )
{
    QList <int> logList;
    queryString = QString ( "SELECT id FROM log WHERE idSoftware = %1" ).arg( inputId );
    query = QSqlQuery  ( queryString );

    while ( query.next() )
        logList.append( query.value( 0 ).toInt() );

    return logList;
}

/**
 * @brief DataBaseWork::selectLogUsingModule
 * Select all item of logs' id in table "Log", using measuging module's id.
 * @param inputId Integer value person's id, which influence to selection id of note in table "Log",
 * @return all integer value of  selected note from column "Id".
 */
QList <int> DataBaseWork::selectLogUsingModule ( int inputId )
{
    QList <int> logList;
    queryString = QString ( "SELECT id FROM log WHERE idMeasuringModule = %1" ).arg( inputId );
    query = QSqlQuery ( queryString );

    while ( query.next() )
        logList.append( query.value( 0 ).toInt() );

    return logList;
}

/**
 * @brief DataBaseWork::selectColumnUsingLog
 * Select all item of parameters' id in table "Parameter", using log's id.
 * @param inputId Integer value log's id, which influence to selection id of note in table "Parameter",
 * @return all integer value of  selected note from column "Id".
 */
QList <int> DataBaseWork::selectParameterUsingLog ( int inputId )
{
    QList <int> columnList;
    queryString = QString ( "SELECT id FROM parameter WHERE idLog = %1" ).arg( inputId );
    query = QSqlQuery ( queryString );

    while ( query.next() )
        columnList.append( query.value( 0 ).toInt() );

    return columnList;
}

/**
 * @brief DataBaseWork::selectDataUsingLog
 * Select all item of data' id in table "Value", using log's id.
 * @param inputId Integer value log's id, which influence to selection id of note in table "Value",
 * @return all integer value of  selected note from column "Id".
 */
QList <int> DataBaseWork::selectDataUsingLog ( int inputId )
{
    QList <int> dataList;
    queryString = QString ( "SELECT id FROM value WHERE idLog = %1" ).arg( inputId );
    query = QSqlQuery ( queryString );

    while ( query.next() )
        dataList.append( query.value( 0 ).toInt() );

    return dataList;
}

/**
 * @brief DataBaseWork::updatePersonId
 * Update values in column "Id" of table "Person".
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::updatePersonId()
{
    queryString = QString( "SELECT COUNT( id ) FROM person" );
    query = QSqlQuery ( queryString );
    query.next();
    idPerson = query.value( 0 ).toInt();

    queryString = QString( "SELECT * FROM person" );
    query = QSqlQuery ( queryString );

    while ( query.next() ) {
        for ( int i = 1; i <= idPerson; i++ ) {
            newId = i;
            oldId = query.value( 0 ).toInt();
            strUpdate = QString( "UPDATE person SET id = %1 WHERE id = %2" ).arg ( newId ).arg( oldId );
            result = queryUpdate.exec( strUpdate );
            if ( !result )
                return result;
        }
    }

    return result;
}

/**
 * @brief DataBaseWork::updateTestId
 * Update values in column "Id" of table "Test".
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::updateTestId()
{
    queryString = QString( "SELECT COUNT( id ) FROM test" );
    query = QSqlQuery ( queryString );
    query.next();
    idTest = query.value( 0 ).toInt();

    queryString = QString( "SELECT * FROM test" );
    query = QSqlQuery ( queryString );

    while ( query.next() ) {
        for ( int i = 1; i <= idTest; i++ ) {
            newId = i;
            oldId = query.value( 0 ).toInt();
            strUpdate = QString( "UPDATE test SET id = %1 WHERE id = %2" ).arg ( newId ).arg( oldId );
            result = queryUpdate.exec( strUpdate );
            if ( !result )
                return result;
        }
    }

    return result;
}

/**
 * @brief DataBaseWork::updateSoftwareId
 * Update values in column "Id" of table "Software".
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::updateSoftwareId()
{
    queryString = QString( "SELECT COUNT( id ) FROM software" );
    query = QSqlQuery ( queryString );
    query.next();
    idSoftware = query.value( 0 ).toInt();

    queryString = QString( "SELECT * FROM software" );
    query = QSqlQuery ( queryString );

    while ( query.next() ) {
        for ( int i = 1; i <= idSoftware; i++ ) {
            newId = i;
            oldId = query.value( 0 ).toInt();
            strUpdate = QString( "UPDATE software SET id = %1 WHERE id = %2" ).arg ( newId ).arg( oldId );
            result = queryUpdate.exec( strUpdate );
            if ( !result )
                return result;
        }
    }

    return result;
}

/**
 * @brief DataBaseWork::updateMeasuringModuleId
 * Update values in column "Id" of table "Measuring module".
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::updateMeasuringModuleId()
{
    queryString = QString( "SELECT COUNT( id ) FROM measuringModule" );
    query = QSqlQuery ( queryString );
    query.next();
    idMeasuringModule = query.value( 0 ).toInt();

    queryString = QString( "SELECT * FROM measuringModule" );
    query = QSqlQuery ( queryString );

    while ( query.next() ) {
        for ( int i = 1; i <= idMeasuringModule; i++ ) {
            newId = i;
            oldId = query.value( 0 ).toInt();
            strUpdate = QString( "UPDATE measuringModule SET id = %1 WHERE id = %2" ).arg ( newId ).arg( oldId );
            result = queryUpdate.exec( strUpdate );
            if ( !result )
                return result;
        }
    }

    return result;
}

/**
 * @brief DataBaseWork::updateLogId
 * Update values in column "Id" of table "Log".
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::updateLogId()
{
    queryString = QString( "SELECT COUNT( id ) FROM log" );
    query = QSqlQuery ( queryString );
    query.next();
    idLog = query.value( 0 ).toInt();

    queryString = QString( "SELECT * FROM log" );
    query = QSqlQuery ( queryString );

    while ( query.next() ) {
        for ( int i = 1; i <= idLog; i++ ) {
            newId = i;
            oldId = query.value( 0 ).toInt();
            strUpdate = QString( "UPDATE log SET id = %1 WHERE id = %2" ).arg ( newId ).arg( oldId );
            result = queryUpdate.exec( strUpdate );
            if ( !result )
                return result;
        }
    }

    return result;
}

/**
 * @brief DataBaseWork::updateParameterId
 * Update values in column "Id" of table "Parameter".
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::updateParameterId()
{
    queryString = QString( "SELECT COUNT( id ) FROM parameter" );
    query = QSqlQuery ( queryString );
    query.next();
    idParameter = query.value( 0 ).toInt();

    queryString = QString( "SELECT * FROM parameter" );
    query = QSqlQuery ( queryString );

    while ( query.next() ) {
        for ( int i = 1; i <= idParameter; i++ ) {
            newId = i;
            oldId = query.value( 0 ).toInt();
            strUpdate = QString( "UPDATE parameter SET id = %1 WHERE id = %2" ).arg ( newId ).arg( oldId );
            result = queryUpdate.exec( strUpdate );
            if ( !result )
                return result;
        }
    }

    return result;
}

/**
 * @brief DataBaseWork::updateValueId
 * Update values in column "Id" of table "Values".
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::updateValueId()
{
    queryString = QString( "SELECT COUNT( id ) FROM value" );
    query = QSqlQuery ( queryString );
    query.next();
    idValue = query.value( 0 ).toInt();

    queryString = QString( "SELECT * FROM value" );
    query = QSqlQuery ( queryString );

    while ( query.next() ) {
        for ( int i = 1; i <= idValue; i++ ) {
            newId = i;
            oldId = query.value( 0 ).toInt();
            strUpdate = QString( "UPDATE Value SET id = %1 WHERE id = %2" ).arg ( newId ).arg( oldId );
            result = queryUpdate.exec( strUpdate );
            if ( !result )
                return result;
        }
    }

    return result;
}

/**
 * @brief DataBaseWork::deletePerson
 * Delete value in table "Person", update values of column "Id" in tables "Person", "Log", "Parameter", "Value".
 * @param inputId Value in column "Id", with what associated note for delete.
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::deletePerson( int inputId )
{
    queryString = QString( "DELETE FROM person WHERE id = %1" ).arg( inputId );
    result = query.exec( queryString );

    if ( result ) {
        updatePersonId();
        updateLogId();
        updateParameterId();
        updateValueId();
    }

    return result;
}

/**
 * @brief DataBaseWork::deleteTest
 * Delete value in table "Test", update values of column "Id" in tables "Test", "Log", "Parameter", "Value".
 * @param inputId Value in column "Id", with what associated note for delete.
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::deleteTest( int inputId )
{
    queryString = QString( "DELETE FROM test WHERE id = %1" ).arg( inputId );
    result = query.exec( queryString );

    if ( result ) {
        updateTestId();
        updateLogId();
        updateParameterId();
        updateValueId();
    }

    return result;
}

/**
 * @brief DataBaseWork::deleteSoftware
 * Delete value in table "Software", update values of column "Id" in tables "Software", "Log", "Parameter", "Value".
 * @param inputId Value in column "Id", with what associated note for delete.
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::deleteSoftware( int inputId )
{
    queryString = QString( "DELETE FROM software WHERE id = %1" ).arg( inputId );
    result = query.exec( queryString );

    if ( result ) {
        updateSoftwareId();
        updateLogId();
        updateParameterId();
        updateValueId();
    }

    return result;
}

/**
 * @brief DataBaseWork::deleteMeasuringModule
 * Delete value in table "Measuring module", update values of column "Id" in tables "Measuring modules", "Log", "Parameter", "Value".
 * @param inputId Value in column "Id", with what associated note for delete.
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::deleteMeasuringModule( int inputId )
{

    queryString = QString( "DELETE FROM measuringModule WHERE id = %1" ).arg( inputId );
    result = query.exec( queryString );

    if ( result ) {
        updateMeasuringModuleId();
        updateLogId();
        updateParameterId();
        updateValueId();
    }

    return result;
}

/**
 * @brief DataBaseWork::deleteParameter
 * Delete value in table "Parameter", update values of column "Id" in tables "Parameter", "Value".
 * @param inputId Value in column "Id", with what associated note for delete.
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::deleteParameter( int inputId )
{
    queryString = QString( "DELETE FROM parameter WHERE id = %1" ).arg( inputId );
    result = query.exec( queryString );

    if ( result ) {
        updateParameterId();
        updateValueId();
    }

    return result;
}

/**
 * @brief DataBaseWork::deleteValue
 * Delete value in table "Value", update values of column "Id" in table "Value".
 * @param inputId Value in column "Id", with what associated note for delete.
 * @return true if query was made correct, else return false.
 */
bool DataBaseWork::deleteValue( int inputId )
{
    queryString = QString( "DELETE FROM Value WHERE id = %1" ).arg( inputId );
    result = query.exec( queryString );

    if ( result )
        updateValueId();

    return result;
}
