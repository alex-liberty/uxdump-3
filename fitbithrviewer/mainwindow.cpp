#include "mainwindow.h"
#include "ui_mainwindow.h"
//#include <iostream>

#define CALLBACK_URL "https://dl.dropboxusercontent.com/u/365114/fitbithr.png"
#define CONFIG_FILE "fitbithrviewer.cfg"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{   qDebug() << "MainWindow()";
    ui->setupUi(this);
    startTime = QDateTime::currentDateTime();
    ui->dateTimeEdit_start->setDisplayFormat("yyyy/MM/dd HH:mm:ss");
    ui->dateTimeEdit_end->setDisplayFormat("yyyy/MM/dd HH:mm:ss");
    ui->dateTimeEdit_start->setDateTime(startTime);
    ui->dateTimeEdit_end->setVisible(false);
    ui->dateTimeEdit_start->setDisabled(true);
    ui->dateTimeEdit_end->setDisabled(true);
    ui->label->setVisible(false);
    /*ui->label->setText(QString("Measurement have been started at ")+startTime.toString("yyyy_MM_dd_HH_mm_ss")+
                       QString("\nand will continue untill the application closing."));*/

    ui->label_3->setVisible(false);

    ui->pushButton_reload->setDisabled(true);
    saveAndExitFlag=false;
    readConfig(); // get client_id and device_id from config file

    //connect(this, SIGNAL(aboutToQuit() ), this, SLOT( saveOnExit() ) );
}


void MainWindow::closeEvent(QCloseEvent *event)
{   qDebug() << "closeEvent()";
    event->ignore();

    // get end time of the experiment:
    endTime = QDateTime::currentDateTime();
    ui->dateTimeEdit_end->setVisible(true);
    ui->dateTimeEdit_end->setDisplayFormat("yyyy/MM/dd HH:mm:ss");
    ui->dateTimeEdit_end->setDateTime(endTime);
    ui->label_3->setVisible(true);
    /*ui->label->setText(QString("Measurement have been started at ")+startTime.toString("yyyy_MM_dd_HH_mm_ss")+
                       QString("\nand have been ended at "+endTime.toString("yyyy_MM_dd_HH_mm_ss"))+".
*/
    ui->label->setText(QString("Please authorize to save data."));
    // sync fitbit data through galileo:
    QProcess process;
    QString cmd;
    if (deviceID.isEmpty()) cmd = "xterm -T \"Sending data from HR meter to fitbit.com...\" -e \"galileo -v --force\"";
    else
    {   cmd = "xterm -T \"Sending data from HR meter to fitbit.com...\" -e \"galileo -v --force -I ";
        cmd.append(deviceID); cmd.append('\"');
    }
    qDebug() << cmd;
    process.start(cmd);
    process.waitForFinished();
    process.close();

    ui->pushButton_reload->setDisabled(false);
    QUrl url(QString("https://www.fitbit.com/oauth2/authorize?response_type=token&client_id=")+clientID+QString("&redirect_uri=" CALLBACK_URL "&scope=activity%20heartrate%20location%20profile%20settings&expires_in=604800"));
    ui->webView->load(url);

}

void MainWindow::on_pushButton_reload_clicked()
{   // for thohse entered wrong login/password we're just reloading authorisation page:

    QUrl url(QString("https://www.fitbit.com/oauth2/authorize?response_type=token&client_id=")+clientID+QString("&redirect_uri=" CALLBACK_URL "&scope=activity%20heartrate%20location%20profile%20settings&expires_in=604800"));
    ui->webView->load(url);
}


void MainWindow::on_webView_urlChanged(const QUrl &url)
{   qDebug() << "urlChanged()";
    qDebug() << url << endl;

    if (!url.isEmpty() && url.toString().startsWith(CALLBACK_URL))
    {   // read access token from the redirect url:
        QString urlWithToken = url.toEncoded();
        urlWithToken = urlWithToken.split("#access_token=").last();
        accessToken = urlWithToken.split("&").first();
        //accessToken = urlWithToken.split("=").at(1);
        qDebug()<<accessToken;
    }

    if (accessToken.isEmpty())      // authorization not done, can't ask for data
    {
       /* if (QMessageBox::Yes == QMessageBox::question(this, "Close Confirmation?",
                       "Authorization not done. Exit without saving data?",
                       QMessageBox::Yes|QMessageBox::No))
        {
                exit(0);
        }*/
        /*// try once again:

        QUrl url(QString("https://www.fitbit.com/oauth2/authorize?response_type=token&client_id=")+clientID+QString("&redirect_uri=" CALLBACK_URL "&scope=activity%20heartrate%20location%20profile%20settings&expires_in=604800"));
        ui->webView->load(url);
        */
    }
    else
    {
        // start acquiring data from fitbit:
        QNetworkAccessManager *manager = new QNetworkAccessManager(this);
        QNetworkRequest request;

        QString separator('/');
        QString urlString=QString("https://api.fitbit.com/1/user/-/activities/heart/date/")+
            startTime.toString("yyyy-MM-dd")+separator+endTime.toString("yyyy-MM-dd")+
            QString("/1sec/time/")+startTime.toString("HH:mm")+separator+
            endTime.toString("HH:mm")+QString(".json");
        qDebug() << urlString;
        request.setUrl(QUrl(urlString));
        request.setRawHeader(QByteArray("Authorization"),QByteArray("Bearer ").append(accessToken));

        connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));
        saveAndExitFlag=true;   // replyFinished function should save log and exit after data acquisition
        manager->get(request);
    }
}

void MainWindow::replyFinished(QNetworkReply* reply)
{   qDebug() << "replyFinished()";
    if (reply->error() == QNetworkReply::NoError)
    {
         QByteArray content= reply->readAll();

         qDebug() << content << endl;

         if (saveAndExitFlag)   // save data to log and exit!
         {
             QJsonDocument jdoc= QJsonDocument::fromJson(content);
             QJsonObject obj = jdoc.object();
             QJsonObject ob2 = obj["activities-heart-intraday"].toObject();
             QJsonArray data = ob2["dataset"].toArray();

             QString filename = QDir::currentPath() + "/Result/";
                     filename.append(endTime.toString("dd_MM_yyyy_HH_mm_ss"));
             filename.append(".fitbithrviewer");
             filename.append(".csv");
             //qDebug() << "file name: " << filename << endl;

             QFile file(filename);
             file.open(QFile::WriteOnly | QFile::Text);
             QTextStream out(&file);
             out << "time,pulse" << endl;
            /* for(auto&& item: data) // a c++11 trick: for each <item> from <data>, data type of <item> autodetected
             {
                    const QJsonObject& datum = item.toObject();
                    out << datum.value("time").toString() << ',' << datum.value("value").toInt() << endl;
           //         qDebug() << datum.value("time").toString() << ',' << datum.value("value").toInt() << endl;
             }*/

             for(int i=0; i<data.size(); i++)
             {
                 const QJsonObject& datum = data[i].toObject();
                 out << datum.value("time").toString() << ',' << datum.value("value").toInt() << endl;
                 //         qDebug() << datum.value("time").toString() << ',' << datum.value("value").toInt() << endl;
             }

             file.close();

             exit(0);
         }

    }
    else
    {
        qDebug()<< reply->errorString();
        ui->label->setText(QString("Measurement have been started at ")+startTime.toString("yyyy_MM_dd_HH_mm_ss")+
                           QString("\nand have been ended at "+endTime.toString("yyyy_MM_dd_HH_mm_ss"))+
                           ". No data acquired from www.fitbit.com due this error: "+reply->errorString()+". Try closing this window for new data acquisition attempt!");


    }

    reply->deleteLater();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::termSignalHandler(int)
{
    char a = 1;
    ::write(sigtermFd[0], &a, sizeof(a));
    //std::cout << "term handler\n";
}


void MainWindow::handleSigTerm()
{
    snTerm->setEnabled(false);
    char tmp;
    ::read(sigtermFd[1], &tmp, sizeof(tmp));

    // do Qt stuff
    close();

    snTerm->setEnabled(true);
}





void MainWindow::readConfig()
{
    QFile file(CONFIG_FILE);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) qDebug() << "file not opened";

    QByteArray content = file.readAll();

    QJsonDocument jdoc= QJsonDocument::fromJson(content);
    QJsonObject obj = jdoc.object();
    clientID=obj["clientID"].toString();
    deviceID=obj["deviceID"].toString();
    qDebug() << clientID << " "<< deviceID << endl;
    file.close();
}



void MainWindow::on_pushButton_quit_clicked()
{
    if (QMessageBox::Yes == QMessageBox::question(this, "Close Confirmation?",
                   "Exit without saving data?",
                   QMessageBox::Yes|QMessageBox::No))
    {
            exit(0);
    }
}

void MainWindow::on_pushButton_extract_clicked()
{   static int extract_now_flag = 0;
    if (extract_now_flag)
    {
        startTime=ui->dateTimeEdit_start->dateTime();
        endTime=ui->dateTimeEdit_end->dateTime();
        close();
    }
    else
    {
        ui->dateTimeEdit_start->setDisabled(false);
        ui->dateTimeEdit_end->setVisible(true);
        ui->label_3->setVisible(true);
        ui->dateTimeEdit_end->setDisabled(false);
        ui->label->setVisible(true);
        ui->label->setText(QString("Set necessary date/time range and click the same \"Get old data\" button once again"));
        extract_now_flag = 1;
    }
}
