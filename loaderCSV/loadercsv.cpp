#include "loadercsv.h"

/**
 * @brief LoaderCSV::LoaderCSV
 * Create a new object of class DataBaseWork and start transaction before add notes.
 */
LoaderCSV::LoaderCSV()
{
    db = new DataBaseWork();
    db->lockTables();
    db->startTransaction();
    insertFile = new QFile("./forInsert.csv");
    id =  db->maxValueId();
id++;
    if ( !insertFile->open( QIODevice::Append | QIODevice::Text ) )
        return;

}

/**
 * @brief LoaderCSV::checkStringValue
 * Add note in table "Value".
 * @param temp Value, using as input parameter data in function addValue;
 * @param column Value, using as input parameter numberColumn in function addValue;
 * @param idLog using as input parameter idLog in function addValue;
 */
void LoaderCSV::checkStringValue(QString &temp, int column, int idLog)
{
    temp.replace("'", apostrof);

    //QThread insert( &insertFile );
    QString insertString = QString::number(id) +  "," +
            QString::number( idLog ) +  ","
            + idParameters[ column ]
            + "," + temp + "\n";
    QTextStream insert (insertFile);
    insert << insertString;
    id++;
    //db->addValue( idLog, Parameters[ column ], temp );
    temp.clear();
    insertFile->flush();
}

/**
 * @brief LoaderCSV::checkStringParameter
 * * Add note in table "Parameter".
 * @param temp Value, using as input parameter name in function addParameter;
 * @param column Value, using as input parameter numberColumn in function addParameter;
 * @param idLog using as input parameter idLog in function addParameter;
 */
void LoaderCSV::checkStringParameter(QString &temp)
{

    temp.replace("'", apostrof);
    Parameters.append( temp );

    idParameters.append(QString::number(db->addParameter( temp )));
    temp.clear();
}

/**
 * @brief LoaderCSV::CSVReader
 * Open file in CSV format, read line, it for elements, using split ',',
 * add elements from the first string in table "Parameter", and other - in table "Value".
 * Function finish transaction and close file at the end.
 * @param idLog Value id of log, with what will be associated new notes in tables
 * @param fileName Name of CSV file for reading
 */
void LoaderCSV::CSVReader( int idLog, QString fileName )
{
    QFile file ( fileName );

    if (file.open(QIODevice::ReadOnly))
    {
        QString data = file.readAll();
        data.remove( QRegExp("\r") ); //remove all ocurrences of CR (Carriage Return)

        QString temp;
        QStringList parserString;

        QTextStream textStream( &data );

        while ( !textStream.atEnd() ) {
            temp = textStream.readLine();
            parserString = temp.split( "," );

            foreach (QString value, parserString) {

                if ( isParameter ) {
                    checkStringParameter ( value );

                    if ( column == parserString.count() - 1 ) {
                        isParameter = false;
                        column = 0;
                    }

                    else
                        column++;
                }

                else {
                    checkStringValue( value, column, idLog );

                    if ( column == parserString.count() - 1 )
                        column = 0;

                    else
                        column++;
                }

            }
        }

    }

    db->loadFromFile();
    file.close();
    db->finishTransaction();
    db->unlockTables();
    insertFile->close();
   // insertFile->remove();
}
