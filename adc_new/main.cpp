#include "adc_new.h"
#include <QApplication>
#include <signal.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    //QIcon appIcon(":/Icons/Icons/serial_port_icon.icns");
    ADC_new w;
    w.show();
    w.setWindowTitle("UXDump");
    w.setMinimumSize(450, 400);

    // black magic to inform ADC_new class about unix SIGTERM signal:
    struct sigaction term;
    term.sa_handler = ADC_new::termSignalHandler;
    sigemptyset(&term.sa_mask);
    term.sa_flags |= SA_RESTART;
    if (sigaction(SIGTERM, &term, 0) > 0) exit(-1);

    return a.exec();
}
