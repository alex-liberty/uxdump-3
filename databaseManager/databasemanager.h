#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QMainWindow>
#include <QMessageBox>
#include <QFileDialog>
#include <QDateTime>

#include "databasework.h"
#include "loadercsv.h"

namespace Ui {
class DatabaseManager;
}

class DatabaseManager : public QMainWindow
{
    Q_OBJECT

public:
    explicit DatabaseManager(QWidget *parent = 0);
    ~DatabaseManager();

private slots:
    void on_comboBoxTest_currentTextChanged( const QString &arg1 );

    void on_pushButtonLoad_clicked();

    void on_comboBoxLogin_currentTextChanged( const QString &arg1 );

    void on_pushButtonDelete_clicked();

    void on_comboBoxLogin_2_currentTextChanged(const QString &arg1);

    void on_comboBoxTest_2_currentTextChanged(const QString &arg1);

private:
    Ui::DatabaseManager *ui;
    void writeDataToForm();
    int readDataFromFormLoad();
    int readDataFromFormDelete();

    void deleteItem();

    DataBaseWork* db;
    LoaderCSV* csv;
    QChar apostrof = QChar(0x2BC);
    QChar apostrof2 = QChar(0x2BA);

    QStringList measuringModules;
    QStringList tests;
    QStringList softwares;
    QStringList logins;
    QList <int> idLogs;
    QStringList dateTimes;

    QString measuringModuleName;
    QString testName;
    QString softwareName;
    QString personLogin;
    QString note;
    QString sex;
    int YOB;
    QDate *date;

    int logId;
    bool result;
};

#endif // DATABASEMANAGER_H
